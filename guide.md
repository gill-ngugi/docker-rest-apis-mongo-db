# DOCKER-MONGODB
---------------------
 1. Set up code on "application.properties" and "docker-compose.yaml"
 
 2. docker ps -a
    docker images
    docker stop mongodb
    docker stop mongo-express
    docker rm mongodb
    docker rm mongo-express
    docker run --name mongodb
    docker run --name mongo-express
    
 3. Run -> docker-compose -f docker-compose.yaml up -d --> or anydocker-compose-fileName.yaml
    * Or run -> docker compose up -d -> Only if you've explicitly named your file as 'docker-compose.yaml'
 
 4. Run docker ps -a
 
 5. Copy the container id of the mongodb image     
 
 6. # MONGO SHELL
    * This will execute into the container that is running the database
      docker exec -it <docker-mongo-db-id> bash
    * This successfully connects you to the database - Mongo Shell -->
      mongo mongodb://localhost:27017 -u root -p password
      
 7. Show databases; OR Show dbs;
    # Create a DB that will be referenced in applications.properties
    use gillCode;
    db.getName(); ...Displays name of created/referenced db; -> Returns gillNewDb;
    db.createCollection("person");
    show collections;
    db.person.stats();
    db.person.drop();
    student = {"id": "1", "name": "Damon"}
    db.person.insert(student);
        OR
        db.person.insert({});
    db.person.count(); //Will return 1
    show collections; //Will return student because it has been added.
    db.student.find(); // Returns the student object.
    db.student.find().pretty(); //Returns the student object prettified.
   
 8. Run application in main class.
   
 9. Execute a request on postman -> on the port 8080
 
 9. docker ps -a
    docker images
    docker stop mongodb
    docker stop mongo-express
    docker rm mongodb
    docker rm mongo-express
    docker run --name mongodb
    docker run --name mongo-express
   