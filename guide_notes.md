# NOTES
Creation of queries in the repository.
https://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/repositories.html

# Query creation from method names
---------------------------------------
public interface PersonRepository extends Repository<User, Long> {
  List<Person> findByEmailAddressAndLastname(EmailAddress emailAddress, String lastname);

  # Enables the distinct flag for the query
  List<Person> findDistinctPeopleByLastnameOrFirstname(String lastname, String firstname);
  List<Person> findPeopleDistinctByLastnameOrFirstname(String lastname, String firstname);

  # Enabling ignoring case for an individual property
  List<Person> findByLastnameIgnoreCase(String lastname);
  # Enabling ignoring case for all suitable properties
  List<Person> findByLastnameAndFirstnameAllIgnoreCase(String lastname, String firstname);

  # Enabling static ORDER BY for a query
  List<Person> findByLastnameOrderByFirstnameAsc(String lastname);
  List<Person> findByLastnameOrderByFirstnameDesc(String lastname);
}

  # Get multiple students by first name and gender
   Repository
   ------------
   List<Student> findStudentsByFirstNameAndGender(String firstName, Gender gender);
   # Service
   ------------
   public List<Student> getStudentsByFnameAndGender(String firstName, Gender gender) {
        return studentRepository.findStudentsByFirstNameAndGender(firstName, gender);
   }
   # Controller
   -------------
   @RequestMapping(value = "{firstName}/{gender}/students", method = RequestMethod.GET)
    public List<Student> fetchStudentsByFnameAndGender(@PathVariable String firstName, Gender gender) {
        return studentService.getStudentsByFnameAndGender(firstName, gender);
    }