package com.example.mongodbrestapi.services;

import com.example.mongodbrestapi.models.Gender;
import com.example.mongodbrestapi.models.Student;
import com.example.mongodbrestapi.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    // Get all students
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    // Get multiple students by first name and gender
    public List<Student> getStudentsByFnameAndGender(String firstName, Gender gender) {
        return studentRepository.findStudentsByFirstNameAndGender(firstName, gender);
    }

    // Get one student by id
    public Student getStudentById(String id) {
        return studentRepository.findById(id).orElse(null);
    }

    // Get one student by email
    public Student getStudentByEmail(String email) {
        return studentRepository.findStudentByEmail(email).orElse(null);
    }

    // Post one student
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    // Post multiple students
    public List<Student> saveStudents(List<Student> students) {
        return studentRepository.saveAll(students);
    }

    // Delete Student by id
    public String deleteStudent(String id) {
        studentRepository.deleteById(id);
        return "Student with ID " + id + " deleted!";
    }

    // Update Student by id
    public Student updateStudent(Student student) {
        Student existingStudent = studentRepository.findById(student.getId()).orElse(null);
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        existingStudent.setGender(student.getGender());
        existingStudent.setAddress(student.getAddress());
        existingStudent.setFavouriteSubjects(student.getFavouriteSubjects());
        existingStudent.setTotalSpentOnBooks(student.getTotalSpentOnBooks());
        return studentRepository.save(existingStudent);
    }

}
