package com.example.mongodbrestapi;

import com.example.mongodbrestapi.models.Address;
import com.example.mongodbrestapi.models.Gender;
import com.example.mongodbrestapi.models.Student;
import com.example.mongodbrestapi.repositories.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class MongodbRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongodbRestApiApplication.class, args);
	}

//	@Bean
//	CommandLineRunner runner(StudentRepository studentRepository) {
//		return args -> {
//			Address address = new Address("England", "Nottingham", "NE9");
//			String email = "jking@abc.com";
//			Student student = new Student(
//				"Jason","King", email, Gender.FEMALE, address,
//				List.of("England", "Canada", "Germany"), BigDecimal.TEN, LocalDateTime.now()
//			);
//
//			studentRepository.findStudentByEmail(email).
//					ifPresentOrElse(s -> {
//						System.out.println(s + " already exists");
//					},
//						() -> {
//							System.out.println("Inserting student " + student);
//							studentRepository.insert(student);
//						}
//					);
//		};
//	}
}
