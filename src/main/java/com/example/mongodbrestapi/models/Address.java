package com.example.mongodbrestapi.models;

import lombok.Data;

@Data
public class Address {
    private String country;
    private String city;
    private String postCode;

    public Address() {

    }

    public Address(String country, String city, String postCode) {
        this.country = country;
        this.city = city;
        this.postCode = postCode;
    }
}
