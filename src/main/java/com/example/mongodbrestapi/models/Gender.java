package com.example.mongodbrestapi.models;

public enum Gender {
    MALE, FEMALE
}
