package com.example.mongodbrestapi.controllers;

import com.example.mongodbrestapi.models.Gender;
import com.example.mongodbrestapi.models.Student;
import com.example.mongodbrestapi.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/students")
    public List<Student> fetchAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping("/students/{firstName}/{gender}")
    public List<Student> fetchStudentsByFnameAndGender(@PathVariable String firstName, @PathVariable Gender gender) {
        return studentService.getStudentsByFnameAndGender(firstName, gender);
    }

    @GetMapping("/student/{id}")
    public Student fetchStudentById(@PathVariable String id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("/studentByEmail/{email}")
    public Student fetchStudentByEmail(@PathVariable String email) {
        return studentService.getStudentByEmail(email);
    }

    @PostMapping("/student")
    public Student createStudent(@RequestBody Student student) {
        return studentService.saveStudent(student);
    }

    @PostMapping("/students")
    public List<Student> createStudents(@RequestBody List<Student> students) {
        return studentService.saveStudents(students);
    }

    @DeleteMapping("/student/{id}")
    public String deleteStudent(@PathVariable String id) {
        return studentService.deleteStudent(id);
    }

    @PutMapping("/student")
    public Student updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }
}
